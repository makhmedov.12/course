package kz.courseonline.dto.response;

import kz.courseonline.model.User;
import lombok.Data;

import java.util.List;

@Data
public class JwtResponse {

    private Long id;
    private String token;
    private String username;
    private String email;
    private List<String> roles;
    private String type = "Bearer";
    private User user;

    public JwtResponse(String accessToken, Long id, String username, String email, List<String> roles, User user) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
        this.user = user;
    }

}
