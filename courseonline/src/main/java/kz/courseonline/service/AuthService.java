package kz.courseonline.service;

import kz.courseonline.dto.request.LoginRequest;
import kz.courseonline.dto.request.SignUpRequest;
import kz.courseonline.dto.response.JwtResponse;
import org.springframework.http.ResponseEntity;

public interface AuthService {

    JwtResponse signIn(LoginRequest loginRequest);
    ResponseEntity<?> signUp(SignUpRequest signupRequest);

}
