package kz.courseonline.service;

import kz.courseonline.model.User;

public interface UserService {

    User getUser(Long id);
    User createUser(User user);

}
