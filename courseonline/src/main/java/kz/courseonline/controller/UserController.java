package kz.courseonline.controller;

import kz.courseonline.model.User;
import kz.courseonline.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    private User getUser(@PathVariable(name = "id") Long userId) {
        return userService.getUser(userId);
    }

    @PostMapping("/create")
    private User createUser(@RequestBody User user) {
        return userService.createUser(user);
    }

}
